package com.example.air.hypeproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * @author Eren ATAS
 * DO NOT FORGET TO ADD YOUR TRACKING ID FROM app/src/res/xml/global_tracker.xml
 */
public class HeartActivity extends AppCompatActivity {

    MainActivity mainAct = new MainActivity();
    Tracker sTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sTracker = mainAct.getDefaultTracker();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart);
        sTracker.setScreenName("Heart Screen"); //Sets the screen name to Heart Screen.
        sTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    /**
     * @brief This method is for triggering the Heart Emoji event, and also to trigger Heart Emoji event.
     * @param view
     */
    public void heartOnClick(View view){
        sTracker = mainAct.getDefaultTracker();
        sTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Moods")
                .setAction("Heart Emoji")
                .build());

        LottieAnimationView animationView = findViewById(R.id.heartanimation_view);
        animationView.playAnimation();
    }

    /**
     * @brief This method is for back button's navigation back to the Main Activity.
     * @param view
     */
    public void backOnClick(View view) {
        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
    }
}
